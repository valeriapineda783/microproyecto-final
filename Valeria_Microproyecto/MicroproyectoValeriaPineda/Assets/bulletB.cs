using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletB : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
  



    // Start is called before the first frame update
    public void Start()
    {
        rb.velocity = transform.right * speed;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {

            Destroy(this.gameObject); 
        }
    }

}
