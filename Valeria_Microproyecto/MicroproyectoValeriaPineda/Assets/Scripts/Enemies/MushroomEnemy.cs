using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomEnemy : MonoBehaviour
{
    public Transform player;
    public float chaseSpeed = 2f;
    public float JumpForce = 2f;
    public LayerMask groundLayer;

    private Rigidbody2D rb;
    private bool isGrounded;
    private bool shouldJump;

    public int damage = 1; 

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // Estamos en el suelo o no?

        isGrounded = Physics2D.Raycast(transform.position, Vector2.down, 1f, groundLayer);

        // Cual es la direci�n del personaje?

        float direction = Mathf.Sign(player.position.x - transform.position.x);

        // Detectamos al personaje?
        bool isPlayerAbove = Physics.Raycast(transform.position, Vector2.up, 5f, 1 << player.gameObject.layer);

        // Si esta tocando el suelo entonces

        if (isGrounded)
        {
            // El enemigo perrsigue al personaje

            rb.velocity = new Vector2(direction * chaseSpeed, rb.velocity.y);

            // Que el enemigo salte si hay una plataforma delante de el o si no tiene m�s plataforma por donde andar delante de el
            // O si el jugador esta encima del enemigo o en una plataforma

            // Si hay plataforma
            RaycastHit2D groundInFront = Physics2D.Raycast(transform.position, new Vector2(direction, 0), 2f, groundLayer);
            // Gap
            RaycastHit2D gapAhead = Physics2D.Raycast(transform.position + new Vector3(direction, 0, 0), Vector2.down, 2f, groundLayer);
            // Si hay una plataforma 
            RaycastHit2D platformAbove = Physics2D.Raycast(transform.position, Vector2.up, 5f, groundLayer); 

            if(!groundInFront.collider && !gapAhead.collider)
            {
                shouldJump = true;
            }
            else if(isPlayerAbove && platformAbove.collider)
            {
                shouldJump = true; 
            }
        }
    }


    private void FixedUpdate()
    {
        if(isGrounded && shouldJump)
        {
            shouldJump = false;
            Vector2 direction = (player.position - transform.position).normalized;

            Vector2 jumpDirection = direction * JumpForce;

            rb.AddForce(new Vector2(jumpDirection.x, JumpForce), ForceMode2D.Impulse); 
        }
    }




}
