using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeTrapEnemy : MonoBehaviour
{

    public float bounceForce = 10f;
   

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.PerderVida();
            HandlePlayerBounce(collision.gameObject);
        }
    }



    private void HandlePlayerBounce(GameObject player)
    {
        Rigidbody2D rb = player.GetComponent<Rigidbody2D>();

        if (rb)
        {
            // Resetear la velocidad de nuestro personaje

            rb.velocity = new Vector2(rb.velocity.x, 0f);


            // hacer que bote el personaje

            rb.AddForce(Vector2.up * bounceForce, ForceMode2D.Impulse);
        }
    }

   

  
}
