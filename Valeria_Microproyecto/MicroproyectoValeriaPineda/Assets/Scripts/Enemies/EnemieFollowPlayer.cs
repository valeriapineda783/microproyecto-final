using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemieFollowPlayer : MonoBehaviour
{
    // Para que el enemigo sea capaz de disparar al jugador

    public float shootingRange;
    public float fireRate= 1f;
    private float nextFireTime; 

    // Para que el enemigo dispare "balas"

    public GameObject bullet;
    public GameObject bulletParent; 

    // Velocidad para el enemigo 
    public float speed;
    // LineOfSite
    public float lineOfSite; 

    // La posici�n del jugador
    private Transform player; 
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform; 
    }

    // Update is called once per frame
    void Update()
    {
        // Pero que el enemigo siga al jugador cuando este se acerque a el

        float distanceFromPlayer = Vector2.Distance(player.position, transform.position);
        if(distanceFromPlayer < lineOfSite && distanceFromPlayer>shootingRange)
        {
            // Que el enemigo siga al jugador
            transform.position = Vector2.MoveTowards(this.transform.position, player.position, speed * Time.deltaTime);
            
        }
        else if(distanceFromPlayer <= shootingRange && nextFireTime < Time.time)
        {
            Instantiate(bullet, bulletParent.transform.position, Quaternion.identity);
            nextFireTime = Time.time + fireRate;
           
        }


        Flip(); 


    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.PerderVida();
        }
    }

    // Para ver el tama�o del lineOfSite hacemos: 

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position,lineOfSite);
        Gizmos.DrawWireSphere(transform.position, shootingRange);
    }

    private void Flip()
    {
        if (transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }


}
