using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prueba : MonoBehaviour
{
    public int vidasEnemigoB = 6;

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("BulletB"))
        {
            vidasEnemigoB -= 1;

            Die(); 

        }
    }


    public void Die()
    {
        if(vidasEnemigoB <= 0)
        {
            Destroy(this.gameObject); 
        }
    }
}
