using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    GameObject target;

    public float speed;

    Rigidbody2D bulletRB; 


    // Start is called before the first frame update
    void Start()
    {
        bulletRB = GetComponent<Rigidbody2D>();
        target = GameObject.FindGameObjectWithTag("Player");
        Vector2 moveDir = (target.transform.position - transform.position).normalized * speed;
        bulletRB.velocity = new Vector2(moveDir.x, moveDir.y);
        Destroy(this.gameObject, 1); 
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("Terrible");

        if (other.gameObject.CompareTag("Player"))
        {

            Destroy(this.gameObject, 0);
           

            GameManager.Instance.PerderVida();
        }

        if (other.gameObject.CompareTag("Ground"))
        {
            Destroy(this.gameObject, 0);
        }

        if (other.gameObject.CompareTag("Wall"))
        {
            Destroy(this.gameObject, 0);
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
