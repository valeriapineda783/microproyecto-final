using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
   

    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("La bala esta tocando al enemigo"); 
        if (collision.gameObject.CompareTag("Enemy"))
        {
            
            Destroy(gameObject); // Destruir la bala al impactar con un enemigo
        }
    }
}
