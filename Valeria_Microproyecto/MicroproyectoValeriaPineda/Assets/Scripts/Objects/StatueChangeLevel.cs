using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StatueChangeLevel : MonoBehaviour
{
    public GameObject patata;
    public GameObject triggerPatata;
    public int numSpiritsActivator = 5;
    

    // Start is called before the first frame update
    void Start()
    {
        patata.SetActive(false);
        triggerPatata.SetActive(true);

       
    }

    private void Update()
    {
        if (ContadorSpirits.currentSpirits >= numSpiritsActivator) {

            triggerPatata.SetActive(false);
            patata.SetActive(true);
        }
    }

    



}
