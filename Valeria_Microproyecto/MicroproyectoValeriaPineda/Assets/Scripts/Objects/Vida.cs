using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vida : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        //etiqueta del elemento con el que ha colisionado la vida

        if (other.gameObject.CompareTag("Player"))
        {
            bool vidaRecuperada = GameManager.Instance.RecuperarVida();


            if (vidaRecuperada)
            {
                //eliminar el objeto una vez se haya recogido
                Destroy(this.gameObject);
            }
           
        } 
    }
    
        
    





}
