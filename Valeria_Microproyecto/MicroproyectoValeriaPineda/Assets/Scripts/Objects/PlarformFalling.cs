using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlarformFalling : MonoBehaviour
{

    private float fallDelay = 1f;

    private float destroyDelay = 2f;

    [SerializeField] private Rigidbody2D rb;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(Fall()); 
        }
    }

    private IEnumerator Fall()
    {
        // tiempo que debemos de esperar desde que el jugador toca la plataforma hasta que la misma cae
        yield return new WaitForSeconds(fallDelay);
        rb.bodyType = RigidbodyType2D.Dynamic;
        // destruimos la plataforma 
        Destroy(gameObject, destroyDelay); 
       
    }
}
