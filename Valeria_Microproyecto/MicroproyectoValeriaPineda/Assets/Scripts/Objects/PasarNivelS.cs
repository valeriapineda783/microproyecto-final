using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PasarNivelS : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip portal;
    public void OnCollisionEnter2D(Collision2D other)
    {

        Debug.Log("Esta tocando");

        if (other.gameObject.CompareTag("Player"))
        {
            Sonidos(); 
            SceneManager.LoadScene(2);
            ContadorSpirits.currentSpirits = 0;
        }
    }

    public void Sonidos()
    {
        audioSource.PlayOneShot(portal);
    }




}
