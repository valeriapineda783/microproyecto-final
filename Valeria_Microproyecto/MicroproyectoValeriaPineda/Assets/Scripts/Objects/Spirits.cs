using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spirits : MonoBehaviour
{
    public int value;
    public AudioSource audioSource;
    public AudioClip coleccionable;

    // si entra en contacto con el elemento cuyo tag es Player
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag ("Player"))
        {
            SonidoPoe(); 

            Destroy(gameObject);
            ContadorSpirits.AumentarSpirits(value); 
        }
    }

    public void SonidoPoe()
    {
        audioSource.PlayOneShot(coleccionable);
    }





}
