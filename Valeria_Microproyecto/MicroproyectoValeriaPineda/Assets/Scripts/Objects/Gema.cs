using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gema : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip gemita; 
    public GameObject gemMessage; 
    public GameObject door; // Referencia a la puerta que se desactivará.

    public void Start()
    {
        gemMessage.SetActive(false); 
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Sonidos(); 
            Destroy(this.gameObject, 0); 
            Collect(); 
        }
    }
    // Método para recoger el objeto.
    public void Collect()
    {
        // Desactiva la puerta.
        door.SetActive(false);

        gemMessage.SetActive(true); 
        
        // Desactiva este objeto.
        gameObject.SetActive(false);

        Destroy(gemMessage, 5f);
    }

    public void Sonidos()
    {
        audioSource.PlayOneShot(gemita); 
    }





}

    

