using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitPlatform : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.TocarLimite();
        }
    }
}
