using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 

public class ContadorSpirits : MonoBehaviour
{

    public TMP_Text spiritText;
    public static int currentSpirits = 0;
    public static ContadorSpirits instance; 

    void Awake()
    {
        instance = this; 
    }
    // Update is called once per frame
    void Start()
    {
        spiritText.text = " " + currentSpirits.ToString(); 

    }

    public static void AumentarSpirits(int v)
    {
        currentSpirits += v;

    }

    private void LateUpdate() 
    {

        spiritText.text = ": " + currentSpirits.ToString();
    }
}
