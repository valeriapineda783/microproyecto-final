using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MensajePuerta : MonoBehaviour
{

    public GameObject mensagePuerta; 
    // Start is called before the first frame update
    void Start()
    {
        mensagePuerta.SetActive(false); 
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            mensagePuerta.SetActive(true);

        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            mensagePuerta.SetActive(false);

        }
    }





}
