using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class GameManager : MonoBehaviour
{

    public AudioSource audioSource;
    public AudioClip da�o;


    public static GameManager Instance { get; private set; }


    public HUD hud;

    public int vidas = 3;
    public int vidasEnemigo = 4;

   
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this; 
        }
        else
        {
            Debug.Log("M�s de un GameManager en la escena"); 
        }
    }


    public void PerderVida()
    {
        SonidoDa�o(); 
        vidas -= 1;

        // A�adir animaci�n 

        if(vidas == 0)
        {

            SceneManager.LoadScene(4);
            ContadorSpirits.currentSpirits = 0; 
        }

        hud.DesactivarVida(vidas); 

    }


    public void EnemigoVida()
    {
        vidasEnemigo -= 1;

        if(vidasEnemigo == 0)
        {
            Destroy(this.gameObject, 0);
        }
    }

    public void TocarLimite()
    {
        // A�adir animaci�n muerte

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        ContadorSpirits.currentSpirits = 0;
    }

    public bool RecuperarVida()
    {
        //ya tiene todas las vidas
        if(vidas == 3)
        {
            return false; 
        }

        hud.ActivarVida(vidas);
        vidas += 1;
        return true; 
        

    }

    public void SonidoDa�o()
    {
        audioSource.PlayOneShot(da�o);
    }


}
