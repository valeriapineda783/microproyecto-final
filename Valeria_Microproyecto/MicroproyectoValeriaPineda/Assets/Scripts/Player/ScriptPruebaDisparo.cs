using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ScriptPruebaDisparo : MonoBehaviour
{

    public Animator animator;
    public ParticleSystem smokeFX;

    public Rigidbody2D rb;

    bool isFacingRight = true;

    // velocidad
    public float moveSpeed = 5f;

    float horizontalMovement;

    // Saber si est�s en el suelo o no 
    public Transform groundCheckPos;

    public Vector2 groundCheckSize = new Vector2(0.5f, 0.05f);

    public LayerMask groundLayer;

    bool isGrounded;

    //Salto 
    public float jumpPower = 10f;
    public int maxJumps = 2;
    int jumpsRemaining;

    // Modificadores de la gravedad

    public float baseGravity = 2;
    public float maxFallSpeed = 18f;
    public float fallGravityMult = 2f;


    // Saber si est�s tocando una pared o no 
    public Transform wallCheckPos;

    public Vector2 wallCheckSize = new Vector2(0.5f, 0.05f);

    public LayerMask wallLayer;

    // Deslizarse por la pared

    public float wallSlideSpeed = 2;
    bool isWallSliding;

    // Saltar por la pared 

    bool isWallJumping;
    float wallJumpDirection;
    float wallJumpTime = 0.5f;
    float wallJumpTimer;
    public Vector2 wallJumpPower = new Vector2(5f, 10f);

    //Para el dash

    private bool canDash = true;
    private bool isDashing;
    private float dashingPower = 24f;
    private float dashingTime = 0.2f;
    private float dashingCooldown = 1f;

    [SerializeField] private TrailRenderer tr;

    // Update is called once per frame
    void Update()
    {
        if (isDashing)
        {
            return;
        }

        GroundCheck();
        ProcessWallSlide();
        ProcessGravity();
        ProcessWallJump();

        if (!isWallJumping)
        {
            rb.velocity = new Vector2(horizontalMovement * moveSpeed, rb.velocity.y);

            Flip();
        }

        animator.SetFloat("yVelocity", rb.velocity.y);
        animator.SetFloat("magnitude", rb.velocity.magnitude);

        if (Input.GetKeyDown(KeyCode.LeftShift) && canDash)
        {
            StartCoroutine(Dash());
        }

        Flip();


    }

    public void Move(InputAction.CallbackContext context)
    {
        horizontalMovement = context.ReadValue<Vector2>().x;
    }

    public void Jump(InputAction.CallbackContext context)
    {
        // Suelo "GroundCheck" saber si el jugador esta tocando el suelo o no 
        if (jumpsRemaining > 0)
        {
            if (context.performed)
            {
                // Si mantenemos pulsado el bot�n sube lo m�ximo
                rb.velocity = new Vector2(rb.velocity.x, jumpPower);
                jumpsRemaining--;
                animator.SetTrigger("jump");
                smokeFX.Play();
            }
            else if (context.canceled && rb.velocity.y > 0)
            {
                // Si pulsamos una vez el bot�n de forma ligera conseguiremos que salte la mitad.
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
                jumpsRemaining--;

                animator.SetTrigger("jump");
                smokeFX.Play();

            }
        }

        // Saltar en la pared

        if (context.performed && wallJumpTimer > 0f)
        {
            isWallJumping = true;

            // Que rebotemos al saltar de una pared 
            rb.velocity = new Vector2(wallJumpDirection * wallJumpPower.x, wallJumpPower.y);
            wallJumpTimer = 0;

            smokeFX.Play();


            animator.SetTrigger("jump");
            // Forzar al personaje a cambiar de posici�n 
            if (transform.localScale.x != wallJumpDirection)
            {
                isFacingRight = !isFacingRight;
                Vector3 ls = transform.localScale;
                ls.x *= -1f;
                transform.localScale = ls;
            }


            // Saltar cada 1 seg

            Invoke("CancelWallJump", wallJumpTime + 0.1f);

        }


    }


    private void GroundCheck()
    {
        if (Physics2D.OverlapBox(groundCheckPos.position, groundCheckSize, 0, groundLayer))
        {
            jumpsRemaining = maxJumps;
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }

    }

    private bool WallCheck()
    {
        return Physics2D.OverlapBox(wallCheckPos.position, wallCheckSize, 0, wallLayer);
    }

    private void ProcessGravity()
    {
        if (rb.velocity.y < 0)
        {
            rb.gravityScale = baseGravity * fallGravityMult;
            rb.velocity = new Vector2(rb.velocity.x, Mathf.Max(rb.velocity.y, -maxFallSpeed));
        }
        else
        {
            rb.gravityScale = baseGravity;
        }
    }


    private void ProcessWallSlide()
    {
        if (!isGrounded & WallCheck() & horizontalMovement != 0)
        {
            isWallSliding = true;
            rb.velocity = new Vector2(rb.velocity.x, Mathf.Max(rb.velocity.y, -wallSlideSpeed));
        }
        else
        {
            isWallSliding = false;
        }
    }

    private void ProcessWallJump()
    {
        if (isWallSliding)
        {
            isWallJumping = false;
            wallJumpDirection = -transform.localScale.x;
            wallJumpTimer = wallJumpTime;


            CancelInvoke("CancelWallJump");
        }
        else if (wallJumpTimer > 0f)
        {
            wallJumpTimer -= Time.deltaTime;
        }
    }

    private void CancelWallJump()
    {
        isWallJumping = false;
    }


    private void Flip()
    {
        if (isFacingRight && horizontalMovement < 0 || !isFacingRight && horizontalMovement > 0)
        {
            isFacingRight = !isFacingRight;
            Vector3 ls = transform.localScale;
            ls.x *= -1f;
            transform.localScale = ls;

            if (rb.velocity.y == 0)
            {
                smokeFX.Play();
            }

        }


    }


    private void OnDrawGizmosSelected()
    {
        // para el grouncheck 
        Gizmos.color = Color.white;

        Gizmos.DrawWireCube(groundCheckPos.position, groundCheckSize);

        // para el wallcheck

        Gizmos.color = Color.blue;

        Gizmos.DrawWireCube(wallCheckPos.position, wallCheckSize);
    }


    //Para el dash

    private IEnumerator Dash()
    {
        canDash = false;
        isDashing = true;
        float originalGravity = rb.gravityScale;
        rb.gravityScale = 0f;
        rb.velocity = new Vector2(transform.localScale.x * dashingPower, 0f);
        tr.emitting = true;
        yield return new WaitForSeconds(dashingTime);
        tr.emitting = false;
        rb.gravityScale = originalGravity;
        isDashing = false;
        // esperar otra vez antes de que pueda hacer otro dash
        yield return new WaitForSeconds(dashingCooldown);
        canDash = true;

    }
}
