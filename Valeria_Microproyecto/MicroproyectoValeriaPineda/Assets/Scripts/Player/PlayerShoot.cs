using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float bulletSpeed = 50f; 

    // Update is called once per frame
    void Update()
    {
        // Si pulsamos el bot�n izquierdo del rat�n 
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }

    void Shoot()
    {
        //Saber la posici�n del rat�n

        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // Direcci�n en la que dispara el enemigo 

        Vector3 shootDirection = (mousePosition - transform.position).normalized;

        GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);

        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(shootDirection.x, shootDirection.y) * bulletSpeed; 

    }


    private void OnCollisionEnter2D(Collision2D other)
    {

        if (other.gameObject.CompareTag("Player"))
        {

            Destroy(this.gameObject, 0);


            GameManager.Instance.EnemigoVida();
        }


    }

}
